# What is this about

This aims to read your `.gitlab-ci.yml` file and process keywords (`include`, `extends`, etc) into a single file, named `.gitlab-ci-all.yml` by default using the main gitlab repo.

## Why is this needed

Because gitlab-runner exec does not support many features right now (August, 2020), and I didn't want to move away from it because of these missing features.

### Notes

I know this may have many issues, mocks are incomplete and I have literally learned ruby in hours to get this working. If you believe you can take this idea and make it better, feel free to do so, just give me some credit for the idea ;)

## Usage

You should mount your project directory to `/gitlab-ci-processor` of container, your `.gitlab-ci.yml` will be processed and outputed as `.gitlab-ci-all.yml` in the same folder.

Example:

```bash
docker container run --rm -i -v $PWD:/gitlab-ci-processor registry.gitlab.com/hehrhart/gitlab-ci-processor/gitlab-processor:latest
# Add source gitlab-ci file and destination of processor merge\
docker container run --rm -i -v $PWD:/gitlab-ci-processor registry.gitlab.com/hehrhart/gitlab-ci-processor/gitlab-processor:latest <source_path> <destination_path>
```

then you can use gitlab-runner executor to run your jobs locally. check down below for more info.

## Create image locally

clone repo then use:

```bash
make install build
```

it downloads the gitlab lib files needed and builds the image from it

then you can run the test using `./make test-processor`

## Current status of `run_job.sh`

It's working now. read below.

### Running pipelines locally

you can use `run_job.sh` script for a quick start on integrating this into your project. I suggest you copy this and edit it to your liking.

Also if anyone think they can make it better, feel free to do so and send a pull request, I happily accept a better approach.

### A note about the `run_job.sh` script

This is a bash script intended to be copied over inside your projects and be used to run the job you specify using this repo's image

```bash
./run_job.sh your-job-name
```

to use current files as is, without affecting the project itself, I copy the project files to /tmp before using it.

I had problems with moving the project files to the containers that are created inside the gitlab-runner container, I have no idea why (probably lack of experience or no good understanding of docker), but I had to use a folder on host to use inside those containers. so I created a folder in /tmp folder of host and moved project files there.

this may or may not cause performance issues in **medium to large projects**. make sure to **edit the `run_job.sh` script** to suit your needs.
