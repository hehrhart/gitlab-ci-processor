FROM ruby:2.6.6-buster AS base

RUN apt-get update -y && apt-get install -y libre2-dev

RUN gem install bundler:2.2.11

WORKDIR /app

COPY ./Gemfile* /app/

RUN bundle install


FROM base AS test

CMD ["./scripts/run_test.sh"]


FROM base AS production

COPY ./scripts/install_gitlab_depedencies.sh /app/scripts/install_gitlab_depedencies.sh

RUN /app/scripts/install_gitlab_depedencies.sh

COPY . /app

VOLUME ["/gitlab-ci-processor"]

WORKDIR /gitlab-ci-processor

ENTRYPOINT ["ruby", "/app/main.rb"]
