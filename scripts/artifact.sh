#!/bin/sh

helper() {
    printf "$1"
    printf "

Command for execute a job.

Argument:
  action                      Action to be execute [setup | extract | include]

Options:
  --job|-j                    Job name
  --artifact-path             Artifact path, use option for each path

Example:
  - ./artifact.sh --job build setup
  - ./artifact.sh --job build --artifact-path ./**/*.min.js --artifact-path /root/text.txt extract

"
}

STOP_LOOP="false"
ARTIFACT_PATHS=""

while [[ $# -gt 0 ]] && [[ "$STOP_LOOP" = "false" ]]
do
    case $1 in
        -h|--help)
            helper "Help:"
            exit 0
        ;;
        --job|-j)
            JOB_NAME="$2"
            shift
            shift
        ;;
        --artifact-path)
            if [ "$ARTIFACT_PATHS" == "" ]; then
                ARTIFACT_PATHS="$2"
            else
                ARTIFACT_PATHS="$ARTIFACT_PATHS $2"
            fi
            shift
            shift
        ;;
        *)
            STOP_LOOP="true"
            ACTION="$1"
        ;;
    esac
done

if [ -z "$JOB_NAME" ]; then
    helper "Error: Missing job name"
    exit 1
fi

CONFIG_FILE_NAME="list.json"
ARTIFACTS_CONFIG_NEW=$(echo "{}" | jq -r '. |= . + {"files": []}')

PROJECT_ARTIFACTS_FOLDER_PATH=".tmp/artifacts/$JOB_NAME"
PROJECT_ARTIFACTS_CONFIG_PATH="$PROJECT_ARTIFACTS_FOLDER_PATH/$CONFIG_FILE_NAME"
PROJECT_ARTIFACTS_BUCKET_PATH="$PROJECT_ARTIFACTS_FOLDER_PATH/bucket"

RUNNER_ARTIFACTS_FOLDER_PATH="/share/artifacts/$JOB_NAME"
RUNNER_ARTIFACTS_CONFIG_PATH="$RUNNER_ARTIFACTS_FOLDER_PATH/$CONFIG_FILE_NAME"
RUNNER_ARTIFACTS_BUCKET_PATH="$RUNNER_ARTIFACTS_FOLDER_PATH/bucket"

setup() {
    if [ ! -d "$PROJECT_ARTIFACTS_FOLDER_PATH" ]; then
        mkdir -p "$PROJECT_ARTIFACTS_FOLDER_PATH"
    fi

    if [ ! -f "$PROJECT_ARTIFACTS_CONFIG_PATH" ]; then
        echo "$ARTIFACTS_CONFIG_NEW" > "$PROJECT_ARTIFACTS_CONFIG_PATH"
    fi

    if [ ! -d "$PROJECT_ARTIFACTS_BUCKET_PATH" ]; then
        mkdir -p "$PROJECT_ARTIFACTS_BUCKET_PATH"
    fi
}

sync_file() {
    local SOURCE="$1"
    local TARGET="$2"
    
    if [ -d "$SOURCE" ]; then
        mkdir -p "$TARGET"
        rsync -av --quiet "$SOURCE/" "$TARGET/"
    else
        mkdir -p "$(dirname "$TARGET")"
        rsync -av --quiet "$SOURCE" "$TARGET"
    fi
}

upload() {
    printf "\nUploading artifacts...\n"

    ARTIFACTS_CONFIG_CURRENT=$(cat "$RUNNER_ARTIFACTS_CONFIG_PATH")

    for ARTIFACT_PATH in $ARTIFACT_PATHS; do
        if [ "${ARTIFACT_PATH:0:1}" != "/" ]; then
            ARTIFACT_PATH=$(cd "$(dirname $ARTIFACT_PATH)"; pwd)/$(basename "$ARTIFACT_PATH")
        fi

        local TARGET_PATH="$ARTIFACT_PATH"
        local ARTIFACT_PATH_RELATIVE_BUILD_FOLDE=$(echo "$ARTIFACT_PATH" | grep -o "/project-0/.*")

        if [ ! -z "$ARTIFACT_PATH_RELATIVE_BUILD_FOLDE" ]; then
            TARGET_PATH="$ARTIFACT_PATH_RELATIVE_BUILD_FOLDE"
        fi

        if [ ! -d "$ARTIFACT_PATH" ] && [ ! -f "$ARTIFACT_PATH" ]; then
            printf "Warning: Artifact with path $ARTIFACT_PATH not found.\n"
        else
            ARTIFACTS_CONFIG_NEW=$(echo "$ARTIFACTS_CONFIG_NEW" | jq \
            --arg bucket "${RUNNER_ARTIFACTS_BUCKET_PATH}${TARGET_PATH}" \
            --arg path "$TARGET_PATH" \
            '.files[.files | length] |= . + {"bucket": $bucket, "path": $path}')

            ARTIFACTS_CONFIG_CURRENT=$(echo "$ARTIFACTS_CONFIG_CURRENT" | jq \
            --arg path "$TARGET_PATH" \
            'del(.files[] | select(.path == $path))')

            sync_file "$ARTIFACT_PATH" "${RUNNER_ARTIFACTS_BUCKET_PATH}${TARGET_PATH}"
        fi
    done

    for ARTIFACTS_DELETE in `echo "$ARTIFACTS_CONFIG_CURRENT" | jq -r '.files[].bucket'`; do
        rm -rf "$ARTIFACTS_DELETE"
    done

    echo "$ARTIFACTS_CONFIG_NEW" > "$RUNNER_ARTIFACTS_CONFIG_PATH"

    find "$RUNNER_ARTIFACTS_BUCKET_PATH" -depth -exec rmdir {} 2>/dev/null \;
}

download() {
    printf "\nDownloading artifacts for $JOB_NAME ...\m"
    for row in $(cat "$RUNNER_ARTIFACTS_CONFIG_PATH" | jq -r '.files[] | @base64'); do
        _jq() {
            echo "${row}" | base64 -d | jq -r "${1}"
        }

        local SOURCE=$(_jq '.bucket')
        local TARGET=$(_jq '.path')

        if [ "${TARGET:0:11}" == "/project-0/" ]; then
            TARGET="$(pwd)/${TARGET:11}"
        fi

        sync_file "$SOURCE" "$TARGET"
    done
}

"$ACTION"
