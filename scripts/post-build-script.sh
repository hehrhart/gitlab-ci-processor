#!/bin/sh

if [ ! "$ARTIFACTS_PATH" == "" ]; then
    ARTIFACTS_PATH="--artifact-path \"$(echo $ARTIFACTS_PATH | sed 's/:/" --artifact-path "/g')\""
    CMD="/scripts/artifact.sh --job "$CI_JOB_NAME" "$ARTIFACTS_PATH" upload"
    eval "$CMD"
fi
