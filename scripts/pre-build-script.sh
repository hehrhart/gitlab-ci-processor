#!/bin/sh

apk add --quiet jq rsync

for ARTIFAT_JOB_FOLDER_NAME in `echo "$ARTIFACTS_NAME" | tr ':' '\n'`; do
    echo "ARTIFAT_JOB_FOLDER_NAME $ARTIFAT_JOB_FOLDER_NAME"
    if [ "$ARTIFAT_JOB_FOLDER_NAME" == "$CI_JOB_NAME" ]; then
        printf "Warning: Job can't unpload own artifact\n"
    else
        /scripts/artifact.sh --job "$ARTIFAT_JOB_FOLDER_NAME" download
    fi
done
