#!/bin/bash

create() {
    docker network create gitlab-runner
    docker volume create minio-export
    docker volume create minio
    docker volume create dind-image
    docker volume create registry-image
    docker volume create registry-mirror-image
    docker container run --rm -v minio-export:/export alpine mkdir -p /export/runner
    docker-compose -f ./docker-compose.yml up -d
}

up() {
    docker-compose -f ./docker-compose.yml up -d
}

down() {
    docker-compose -f ./docker-compose.yml down
}

destroy() {
    down
    docker volume rm minio-export
    docker volume rm minio
    docker volume rm dind-image
    docker volume rm registry-image
    docker volume rm registry-mirror-image
    docker network rm gitlab-runner
}

"$1" "${@:2}"