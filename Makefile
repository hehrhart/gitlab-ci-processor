install:
	bundle config --local path vendor
	bundle install -j $$(nproc)
	./scripts/install_gitlab_depedencies.sh

install-depedencies-job-run:
	./scripts/job_setup.sh create

start:
	./scripts/job_setup.sh up

stop:
	./scripts/job_setup.sh down

clean:
	./scripts/job_setup.sh destroy

test-processor:
	docker container run --rm \
	-v $$PWD:/app \
	gitlab-ci-processor:test

build-test:
	docker build --target=test -t gitlab-ci-processor:test .

build:
	docker build --target=production -t gitlab-ci-processor:latest .

docker-registry-list-images:
	@echo "Image in local docker registry"
	curl http://$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker-compose ps -q registry)):5000/v2/_catalog
	@echo "Image in local docker mirror registry"
	curl http://$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker-compose ps -q registry-mirror)):5000/v2/_catalog

.SILENT: test-processor
.SILENT: docker-registry-list-images
