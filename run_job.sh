#!/bin/bash

# exit if docker command is not available
!(type -P "docker" &>/dev/null || type -P "docker.exe" &>/dev/null) && echo "docker not found" && exit 1

UNAME_OUT="$(uname -s)"

case "${UNAME_OUT}" in
    Linux*)     OS=Linux;;
    Darwin*)    OS=Mac;;
    CYGWIN*)    OS=Cygwin;;
    MINGW*)     OS=MinGw;;
    *)          OS="UNKNOWN:${unameOut}"
esac

helper() {
    printf "$1"
    printf "

Command for execute a job.

Argument:
  name                        Job name

Options:
  --env|-e                    Environment variable passed to the job
  --debug                     Add debug option
  --skip-merge-file           Skip gitlab-ci file merge
  --artifact-name             Include atrifact from a job, use job name has value
  --artifact-path             Upload in artifact

Example:
  - ./gitlab-runner job --env CI_COMMIT_REF_SLUG=staging lint
  - ./gitlab-runner job --artifact-path './docker-images' build
  - ./gitlab-runner job --artifact-name build push

"
}

STOP_LOOP="false"
ENVIRONMENT_VARIABLES=()
ARTIFACTS_NAME=""
ARTIFACTS_PATH=""
DEBUG=0
SKIP_MERGE_FILE=0

while [[ $# -gt 0 ]] && [[ "$STOP_LOOP" = "false" ]]
do
  case $1 in
    -h|--help)
      helper "Help:"
      exit 0
    ;;
    --env|-e)
      if [ ${#ENVIRONMENT_VARIABLES[@]} -eq 0 ]; then
        ENVIRONMENT_VARIABLES=("--env $2")
      else
        ENVIRONMENT_VARIABLES=("${ENVIRONMENT_VARIABLES[@]}" "--env $2")
      fi
      shift
      shift
    ;;
    --artifact-name)
      if [ "$ARTIFACTS_NAME" == "" ]; then
        ARTIFACTS_NAME="$2"
      else
        ARTIFACTS_NAME="$ARTIFACTS_NAME:$2"
      fi
      shift
      shift
    ;;
    --artifact-path)
      if [ "$ARTIFACTS_PATH" == "" ]; then
        ARTIFACTS_PATH="$2"
      else
        ARTIFACTS_PATH="$ARTIFACTS_PATH:$2"
      fi
      shift
      shift
    ;;
    --debug)
      DEBUG=1
      shift
    ;;
    --skip-merge-file)
      SKIP_MERGE_FILE=1
      shift
    ;;
    *)
      STOP_LOOP="true"
      JOB_NAME="$1"
    ;;
  esac
done

# check if job name is provided
if [ -z "$JOB_NAME" ]; then
    helper "Error: Missing job name"
    exit 1
fi

# fixes issues of docker in windows git bash
if [ "$OSTYPE" = "msys" ]; then
  docker() {
    (export MSYS_NO_PATHCONV=1; "docker.exe" "$@")
  }
fi

RUNNER_COMMANDE_OPTIONS=""

# if "debug" is set, pass it to gitlab-runner
((DEBUG)) && RUNNER_COMMANDE_OPTIONS="$RUNNER_COMMANDE_OPTIONS --debug --log-level debug"

printf "Create project and copy file ...\n"

# get project name from git command
project_name=$(git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p')

GITLAB_RUNNER_FOLDER="/tmp/gitlab-runner-tmp"
PROJECT_FOLDER="$GITLAB_RUNNER_FOLDER/$project_name"
GITLAB_CI_MERGED_FILE_PATH="$GITLAB_RUNNER_FOLDER/gitlab-ci-all.yml"

if [ ! -d "$GITLAB_RUNNER_FOLDER" ]; then
  mkdir -p "$GITLAB_RUNNER_FOLDER"
fi

if [ ! -d "$PROJECT_FOLDER" ]; then
  mkdir "$PROJECT_FOLDER"
fi

if ((DEBUG)); then
  rsync -av --progress --verbose . "$PROJECT_FOLDER"
else
  rsync -avq . "$PROJECT_FOLDER" --delete
fi

if [ ! -f "$GITLAB_CI_MERGED_FILE_PATH" ] || ! ((SKIP_MERGE_FILE)); then
  printf "Merge gitlab-ci files ...\n"

  docker run --rm -i -v $PROJECT_FOLDER:/gitlab-ci-processor registry.gitlab.com/hehrhart/gitlab-ci-processor/gitlab-processor:latest

  mv "$PROJECT_FOLDER/.gitlab-ci-all.yml" "$GITLAB_CI_MERGED_FILE_PATH"

  if [ "$OS" == "Mac" ]; then
    gsed -i "s/\$CI_JOB_TOKEN/\$CI_JOB_TOKEN_UNPROTECTED/g" "$GITLAB_CI_MERGED_FILE_PATH"
  else
    sed -i "s/\$CI_JOB_TOKEN/\$CI_JOB_TOKEN_UNPROTECTED/g" "$GITLAB_CI_MERGED_FILE_PATH"
  fi
fi

rm -rf "$PROJECT_FOLDER/.gitlab-ci.yml"
cp "$GITLAB_CI_MERGED_FILE_PATH" "$PROJECT_FOLDER/.gitlab-ci.yml"

printf "Prepare runner ...\n"

CI_JOB_TEST_ID=$(uuidgen)

# set runner environment variables
ENVIRONMENT_VARIABLES_ARGS_DEFAULT=(
  "CI_PROJECT_TITLE=$project_name"
  "CI_PROJECT_NAME=$project_name"
  "DOCKER_HOST=tcp://$DIND_NAME:2375"
  "DOCKER_TLS_CERTDIR=\"\""
  "DOCKER_DRIVER=overlay2"
  "CI_JOB_TEST_ID=$CI_JOB_TEST_ID"
  "CI_REGISTRY=registry.domain.local"
  "CI_JOB_TOKEN=token"
  "ARTIFACTS_NAME=$ARTIFACTS_NAME"
  "ARTIFACTS_PATH=$ARTIFACTS_PATH"
);

ENVIRONMENT_VARIABLES_ARGS=()

# make the statement
for ENVIRONMENT_VARIABLE in "${ENVIRONMENT_VARIABLES_ARGS_DEFAULT[@]}"; do
  ENVIRONMENT_VARIABLES_ARGS=("${ENVIRONMENT_VARIABLES_ARGS[@]}" "--env $ENVIRONMENT_VARIABLE")
done

ENVIRONMENT_VARIABLES_ARGS=("${ENVIRONMENT_VARIABLES_ARGS[@]}" "${ENVIRONMENT_VARIABLES[@]}")

for ENVIRONMENT_VARIABLE in "${ENVIRONMENT_VARIABLES_ARGS[@]}"; do
  if [[ "$ENVIRONMENT_VARIABLE" =~ " CI_JOB_TOKEN=" ]]; then
    ENVIRONMENT_VARIABLES_ARGS=("${ENVIRONMENT_VARIABLES_ARGS[@]}" "--env CI_JOB_TOKEN_UNPROTECTED=$(echo $ENVIRONMENT_VARIABLE | sed -E 's/^--env CI_JOB_TOKEN=(.*)$/\1/')")
  fi
done

if [ "$ARTIFACTS_PATH" != "" ]; then
  ./scripts/artifact.sh --job "$JOB_NAME" setup
fi

# eliminate the need for double slash in mounting the socket on windows, I guess
export COMPOSE_CONVERT_WINDOWS_PATHS=1;
# give gitlab-runner image access to docker daemon
docker_volumes="-v /var/run/docker.sock:/var/run/docker.sock";
# and a volume to pass between jobs
docker_volumes="$docker_volumes -v $GITLAB_RUNNER_FOLDER:$GITLAB_RUNNER_FOLDER"
# override entrypoint to use our custom commands
docker_entrypoint="--entrypoint /bin/bash";
# gitlab-runner image (use the one you prefer)
# runner_image="gitlab/gitlab-runner"
runner_image="gitlab/gitlab-runner:alpine";
# combine docker args
docker_cmd_flags="$docker_volumes $docker_entrypoint $runner_image"

BUILD_FOLDER="/builds/$(uuidgen)"

# creates a container and returns it's id
create_job() {
  local job_name=$1;
  docker_command="docker create $docker_cmd_flags \
    -c ' \
      cd $PROJECT_FOLDER;
      gitlab-runner $RUNNER_COMMANDE_OPTIONS exec docker
        --docker-privileged
        --docker-network-mode gitlab-runner
        --clone-url \"file://$PROJECT_FOLDER\"
        --docker-links $DIND_NAME
        --docker-links $MINIO_NAME
        --docker-links $REGISTRY_NAME
        --cache-type=s3
        --cache-shared=true
        --cache-s3-server-address=$MINIO_NAME:9000
        --cache-s3-access-key=$MINIO_ACCESS_KEY
        --cache-s3-secret-key=$MINIO_SECRET_KEY
        --cache-s3-bucket-name=runner
        --cache-s3-insecure=true
        --builds-dir $BUILD_FOLDER
        --docker-volumes '$PROJECT_FOLDER/scripts:/scripts'
        --docker-volumes '$PWD/.tmp/artifacts:/share/artifacts'
        --pre-build-script '/scripts/pre-build-script.sh'
        --post-build-script '/scripts/post-build-script.sh'
        ${ENVIRONMENT_VARIABLES_ARGS[@]}
        $job_name; 
    '";

  local container_id=$(eval $docker_command);
  # echo container id
  echo $container_id;
}

# create gitlab runner
dcid=$(create_job $JOB_NAME);
# starts the container and wait for it to finish using -i
docker start $dcid;
docker container logs -f "$dcid"
job_result=$?;
# remove the container

printf "\nCLEAN runner ...\n"

docker container rm -f "$dcid"

CLEAN_CONTAINER_JOB_TEST_ID=()
TMP_CONTAINER=()

for CONTAINER_ID in `docker ps -aqf "name=^runner--project-0"`; do
  JOB_TEST_ID=$(docker inspect -f "{{ .Config.Env }}" $CONTAINER_ID | tr ' ' '\n' | grep CI_JOB_TEST_ID | sed 's/^.*=//')
  if [ ! -z "$JOB_TEST_ID" ] && [ "$JOB_TEST_ID" == "$CI_JOB_TEST_ID" ]; then
    CLEAN_CONTAINER_JOB_TEST_ID=("${CLEAN_CONTAINER_JOB_TEST_ID[@]}" "$CONTAINER_ID")
  else
    TMP_CONTAINER=("${TMP_CONTAINER[@]}" "$CONTAINER_ID")
  fi
done

for CONTAINER_ID in ${CLEAN_CONTAINER_JOB_TEST_ID[@]}; do
  NEW_CONTAINER_TMP=()
  for CONTAINER_TMP_ID in ${TMP_CONTAINER[@]}; do
    if [ $(docker inspect -f "{{ .HostConfig.Links }}" $CONTAINER_TMP_ID | grep -c $(docker inspect -f "{{ .Name }}" $CONTAINER_ID)) -ne 0 ]; then
      docker container rm -f "$CONTAINER_TMP_ID"
    else
      NEW_CONTAINER_TMP=("${NEW_CONTAINER_TMP[@]}" "$CONTAINER_TMP_ID")
    fi
  done
  TMP_CONTAINER=("${NEW_CONTAINER_TMP[@]}")
  docker container rm -f "$CONTAINER_ID"
done

red_esc="\e[1;31m"
green_esc="\e[1;32m"
reset_esc="\e[0m"

if [ "$job_result" -eq 0 ]; then
  echo -e "$green_esc"
  echo -e "############################\n#### Job succeeded\n############################"
  echo -e "$reset_esc"
else
  echo -e "$red_esc"
  echo -e "############################\n#### Job failed: exit code $job_result\n############################"
  echo -e "$reset_esc"
fi;
